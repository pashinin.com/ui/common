/* eslint-disable no-param-reassign */

const mutations = {
  // Indiates if service worker got an update
  setNewUpdate(state: any, worker: any) {
    state.hasUpdate = true;
    state.newWorker = worker;
  },
  setNoUpdate(state: any) {
    state.hasUpdate = false;
    state.newWorker = undefined;
  },
};

export default {
  // namespaced: true,
  state: {
    hasUpdate: false,
    newWorker: undefined,
  },
  mutations,
};
