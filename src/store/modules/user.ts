/* eslint-disable no-param-reassign */
import Vue from 'vue';


const mutations = {
  /**
   * Makes whole app know the user is logged in.
   * Runs after user got JWT token.
   * @param {data} object JSON data (user fields) returned by API in /auth/signin
   */
  LOGIN_SUCCESS(state: any, data: any) {
    state.email = data.email;
    state.id = data.id;
    state.firstname = data.firstname;
    state.lastname = data.lastname;
    state.authenticated = true;
  },
  LOGOUT(state: any) {
    state.token = null;
    state.email = null;
    state.authenticated = false;
  },
};


export default {
  state: {
    email: null,
    firstname: null,
    lastname: null,
    authenticated: false,
  },
  getters: {
    // authenticated(state) {
    //   return state.currentNote;
    // }
  },
  mutations,
  // plugins: [vuexLocal.plugin],
};
