import { SentryVue } from './sentry';
import AppUpdate from './AppUpdate.vue';
import pwa from './store/modules/pwa';

export {
  AppUpdate,
  SentryVue,
  pwa,
};

export default SentryVue;
