import * as Sentry from '@sentry/browser';
// import * as Integrations from '@sentry/integrations';
import { Vue as VueIntegration } from '@sentry/integrations';


function SentryVue(Vue: any, projectName?: string) {
  if (process.env.NODE_ENV !== 'production') {
    return;
  }
  if (!process.env.VUE_APP_SENTRY_DSN) {
    return;
  }
  const project = projectName || process.env.VUE_APP_NAME;
  try {
    Sentry.init({
      // environment: 'dev',
      release: `${project}@${process.env.VUE_APP_VERSION}`,
      dsn: process.env.VUE_APP_SENTRY_DSN,
      integrations: [new VueIntegration({ Vue, attachProps: true })],
    });
  } catch (e) {
    console.log(e);
  }
}

export {
  SentryVue,
};


export default SentryVue;
